'use strict';

/* Controllers */

angular.module('myApp.controllers', []).
  controller('MyCtrl1', [function() {

  }])
  .controller('MyCtrl2', [function() {

  }])
  .controller('tictactoeCtrl',['$scope','tictactoeCols',function ($scope, tictactoeCols) {
	$scope.tictactoeCols	= [[' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' ']];
	$scope.input1			= 'X';
	$scope.input2			= 'O';

	$scope.lastClicked  	= $scope.input1;
	$scope.message		  	= '';

	$scope.won				= false;
	$scope.fullFilled		= false;
	$scope.result			= new Array();

	$scope.players			= new Array();
	$scope.players[0]		= 'Player 1';
	$scope.players[1]		= 'Player 2';




	$scope.clickedInCell = function (row, col) {

		if(true == $scope.won || true == $scope.fullFilled) return;

		if( ' ' != $scope.tictactoeCols[row][col]) return;
		
		$scope.tictactoeCols[row][col]	= $scope.lastClicked;
		$scope.lastClicked				= $scope.lastClicked==$scope.input1?$scope.input2:$scope.input1;

		//Horizontal
		for(var i =0; i < 3; i++)
		{
			if(($scope.input1 == $scope.tictactoeCols[i][0]) && ($scope.input1 == $scope.tictactoeCols[i][1]) && ($scope.input1 == $scope.tictactoeCols[i][2]))
			{
				$scope.won 		= true; 
				$scope.message	= $scope.players[0] + ' won';
			}else if(($scope.input2 == $scope.tictactoeCols[i][0]) && ($scope.input2== $scope.tictactoeCols[i][1]) && ($scope.input2 == $scope.tictactoeCols[i][2]))
			{
				$scope.won 		= true; 
				$scope.message	= $scope.players[1] + ' won';
			}
		}

		//Vertical
		for(var i =0; i < 3; i++)
		{
			if(($scope.input1 == $scope.tictactoeCols[0][i]) && ($scope.input1 == $scope.tictactoeCols[1][i]) && ($scope.input1 == $scope.tictactoeCols[2][i]))
			{
				$scope.won 		= true; 
				$scope.message	= $scope.players[0] + ' won';
			}else if(($scope.input2 == $scope.tictactoeCols[0][i]) && ($scope.input2 == $scope.tictactoeCols[1][i]) && ($scope.input2 == $scope.tictactoeCols[2][i]))
			{
				$scope.won 		= true; 
				$scope.message	= $scope.players[1] + ' won';
			}
		}

		//Diagonal
		if(($scope.input1 == $scope.tictactoeCols[0][0]) && ($scope.input1 == $scope.tictactoeCols[1][1]) && ($scope.input1 == $scope.tictactoeCols[2][2]))
		{
			$scope.won 		= true; 
			$scope.message	= $scope.players[0] + ' won';
		}else if(($scope.input2 == $scope.tictactoeCols[0][0]) && ($scope.input2 == $scope.tictactoeCols[1][1]) && ($scope.input2 == $scope.tictactoeCols[2][2]))
		{
			$scope.won 		= true; 
			$scope.message	= $scope.players[1] + ' won';
		}

		if(($scope.input1 == $scope.tictactoeCols[2][0]) && ($scope.input1 == $scope.tictactoeCols[1][1]) && ($scope.input1 == $scope.tictactoeCols[0][2]))
		{
			$scope.won 		= true; 
			$scope.message	= $scope.players[0] + ' won';
		}else if(($scope.input2 == $scope.tictactoeCols[2][0]) && ($scope.input2 == $scope.tictactoeCols[1][1]) && ($scope.input2 == $scope.tictactoeCols[0][2]))
		{
			$scope.won 		= true; 
			$scope.message	= $scope.players[1] + ' won';
		}

		$scope.fullFilled = true;

		for(var i =0; i < 3; i++)
		{
			for(var j =0; j < 3; j++)
			{
				if(' ' == $scope.tictactoeCols[i][j])				
				{
					$scope.fullFilled = false;
				}
			}
		}

		if(true == $scope.fullFilled)
		{
			$scope.message	= 'Match tied';	
		}
	}

  }]);