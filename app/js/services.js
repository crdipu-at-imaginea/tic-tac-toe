'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('myApp.services', [])
	.value('version', '0.1')
  	.value('tictactoeCols', [['1','2','3'], ['4','5','6'], ['7','8','9']]);
